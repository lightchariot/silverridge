import argparse

import pandas as pd

from common.utils import make_data
from engine.models import odds_from_dict
from engine.poisson import weighted_mean, calculate_internal


def get_previous_matches(results, team, is_home):
    previous = []
    for match in results:
        if is_home and match['home_team'] == team:
            previous.append(match)
        elif not is_home and match['away_team'] == team:
            previous.append(match)
    return previous

def league_means(results, decay):
    home_goals = [int(m['home_goals']) for m in results]
    away_goals = [int(m['away_goals']) for m in results]
    return weighted_mean(home_goals, decay), weighted_mean(away_goals, decay)

def calculate(match, results, is_home, decay):
    if is_home:
        team = match['home_team']
    else:
        team = match['away_team']

    prev = get_previous_matches(results, team, is_home=is_home)
    prev.reverse()
    home_goals = [int(m['home_goals']) for m in prev]
    away_goals = [int(m['away_goals']) for m in prev]
    return weighted_mean(home_goals, decay), weighted_mean(away_goals, decay)


def run_it(results, upcoming, decay):
    data = {
        'Home Team': [], 'Away Team': [],
        'E(home)': [], 'E(draw)': [], 'E(away)': [],
        'E(over)': [], 'E(under)': [],
        'home_odds': [], 'draw_odds': [], 'away_odds': [],
        'over_odds': [], 'under_odds': [], 'ou_line': [],
    }

    for match in upcoming:
        league_h_wt_mean, league_a_wt_mean = league_means(results, decay)
        h_scored, h_allowed = calculate(match, results, True, decay)
        h_scored /= league_h_wt_mean
        h_allowed /= league_a_wt_mean

        a_allowed, a_scored = calculate(match, results, False, decay)
        a_allowed /= league_h_wt_mean
        a_scored /= league_a_wt_mean

        h_xg = h_scored * a_allowed * league_h_wt_mean
        a_xg = a_scored * h_allowed * league_a_wt_mean

        odds = odds_from_dict(match)
        evs = calculate_internal(h_xg, a_xg, odds)
        # ties_allowed=False

        data['Home Team'].append(match['home_team'])
        data['Away Team'].append(match['away_team'])
        for k, v in evs.items():
            data[k].append(v)
        data['home_odds'].append(float(match['home_odds']))
        data['draw_odds'].append(float(match['draw_odds']))
        data['away_odds'].append(float(match['away_odds']))
        data['over_odds'].append(float(match['over_odds']))
        data['under_odds'].append(float(match['under_odds']))
        data['ou_line'].append(float(match['ou_line']))

    return pd.DataFrame.from_dict(data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--all_matches_file', type=str, required=True)
    parser.add_argument('-m', '--match_odds_file', type=str, required=True,
                        help='Path to match odds CSV file')
    parser.add_argument('-d', '--decay', type=float, default=0.0)
    # parser.add_argument('-t', '--ties_allowed', dest='ties_allowed',
    #                     action='store_false')
    # parser.set_defaults(ties_allowed=True)

    args = parser.parse_args()
    decay = args.decay

    results = make_data(args.all_matches_file)
    upcoming = make_data(args.match_odds_file)
    df = run_it(results, upcoming, decay)
    print(df.round(6).to_csv(sep='\t'))
