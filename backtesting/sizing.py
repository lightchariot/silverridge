def kelly(p_win, odds, p_lose=None):
	if not p_lose:
		p_lose = 1 - p_win
	return p_win - p_lose / (odds - 1)
