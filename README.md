# silverridge

[![pipeline status](https://gitlab.com/lightchariot/silverridge/badges/main/pipeline.svg)](https://gitlab.com/lightchariot/silverridge/-/commits/main) [![coverage report](https://gitlab.com/lightchariot/silverridge/badges/main/coverage.svg)](https://gitlab.com/lightchariot/silverridge/-/commits/main)

Sports modeling playground

Head over to the wiki for more information and documentation.
