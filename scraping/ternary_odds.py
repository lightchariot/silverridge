import bs4

from common.utils import parse_html
from common.constants import BOOKMAKER_ID, ODDS_TAG


def _make_ternary_odds_url(match_id):
    return f'https://www.betexplorer.com/match-odds/{match_id}/1/1x2/odds/'


def get(match_url):
    groups = match_url.split('/')
    match_id = groups[-2]

    odds_url = _make_ternary_odds_url(match_id)
    soup = parse_html(odds_url)

    odds = soup.find_all('tr', {'data-bid': BOOKMAKER_ID})
    if not odds:
        return None
    children = [c for c in odds[0].children if isinstance(c, bs4.Tag)]
    return [c[ODDS_TAG] for c in children if c.has_attr(ODDS_TAG)]
