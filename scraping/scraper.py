import csv
import random
import re
import time
import urllib.parse

from common.commandline import make_arguments
from common.constants import BASE_URL, LEAGUES
from scraping import match_urls
from scraping.formatting import format_data
from scraping.odds_and_result import get_odds_and_result


def write_to_csv(filename, data, header):
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(header)
        writer.writerows(data)


if __name__ == '__main__':
    args = make_arguments()
    league = args.league
    country = LEAGUES[league]
    season = args.season

    if not season:
        season = '2024-25'
        results_url = f'https://www.betexplorer.com/soccer/{country}/{league}/results/'
    else:
        results_url = f'https://www.betexplorer.com/football/{country}/{league}-{season}/results/'

    if not re.fullmatch('[0-9]{4}-[0-9]{2}', season):
        raise ValueError('Season must be in the format YYYY-YY; e.g. 2024-25')

    # Sort matchpages by earliest to latest date.
    matchpages = match_urls.get(results_url)
    matchpages.reverse()

    data = []
    for matchpage in matchpages:
        home = matchpage[0]
        away = matchpage[1]
        match_url = urllib.parse.urljoin(BASE_URL, matchpage[2])
        print(home, away, match_url)
        ternary, result, ou_odds = get_odds_and_result(match_url)
        if not ternary or not result or not ou_odds:
            print(f'Skipping {home} - {away}.')
            continue
        data.append(format_data(home, away, ternary, result, ou_odds))

        # Avoid rate-limiting.
        delay_sec = 0.3 + 0.1 * random.randint(0, 5)
        time.sleep(delay_sec)

    data_header = ['kickoff',
                   'home_team', 'away_team', 'home_goals', 'away_goals',
                   'home_odds', 'draw_odds', 'away_odds',
                   'ou_line', 'over_odds', 'under_odds']
    write_to_csv(f'{league}-{season}.csv', data, data_header)
