from common.constants import DATE_FORMAT


def format_row(home, away, ternary, start, ou_odds):
    ou_line = list(ou_odds.keys())[0]
    ou_tuple = [ou_line] + ou_odds[ou_line]
    kickoff = start.strftime(DATE_FORMAT)
    return [kickoff, home, away] + ternary + ou_tuple


def format_float(f_str):
    return round(float(f_str), 2)


def format_data(home, away, ternary, result, ou_odds):
    ou_line = list(ou_odds.keys())[0]
    ou_tuple = [ou_line] + ou_odds[ou_line]
    kickoff = result['start'].strftime(DATE_FORMAT)
    return [kickoff, home, away, result['home'], result['away']] + ternary + ou_tuple
