import csv
import urllib
from datetime import datetime, timedelta

import pytz
import requests
from bs4 import BeautifulSoup

from common.constants import LEAGUES, BASE_URL
from common.utils import make_data
from common.commandline import make_arguments
from newkenny import run_it
from scraping import match_urls, over_under_odds, ternary_odds, results
from scraping.formatting import format_row


def write_to_csv(filename, data, header):
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(header)
        writer.writerows(data)


if __name__ == '__main__':
    print('Upcoming Matches:')
    args = make_arguments()
    league = args.league
    country = LEAGUES[league]
    url = f'https://betexplorer.com/football/{country}/{league}/'

    fixtures_tag = 'table-main table-main--leaguefixtures h-mb15'
    now = datetime.now(tz=pytz.UTC)
    next_matches = match_urls.get(url)

    tomorrow = now + timedelta(days=1)
    upcomings = []
    for matchpage in next_matches:
        home = matchpage[0]
        away = matchpage[1]
        suburl = matchpage[2]
        match_url = urllib.parse.urljoin(BASE_URL, matchpage[2])
        ou_odds = over_under_odds.get(match_url)
        ternary = ternary_odds.get(match_url)

        r = requests.get(match_url)
        soup = BeautifulSoup(r.text, 'html.parser')
        start = results.get_start(soup)
        if start > tomorrow or start < now:
            break
        row = format_row(home, away, ternary, start, ou_odds)
        upcomings.append(row)

    season = args.season

    odds_header = ['kickoff', 'home_team', 'away_team',
                   'home_odds', 'draw_odds', 'away_odds',
                   'ou_line', 'over_odds', 'under_odds']
    upcoming_file = 'upcoming.csv'
    write_to_csv(upcoming_file, upcomings, odds_header)

    league_file = f'../backtesting/data/{league}-{season}.csv'
    results = make_data(league_file)
    upcomings = make_data(upcoming_file)
    df = run_it(results, upcomings, 0.03)
    print(df.round(6).to_csv(sep='\t'))
