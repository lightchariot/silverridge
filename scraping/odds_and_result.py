from scraping import ternary_odds, results, over_under_odds


def get_odds_and_result(match_url):
    ternary = ternary_odds.get(match_url)
    result = results.get(match_url)
    ou_odds = over_under_odds.get(match_url)
    return ternary, result, ou_odds
