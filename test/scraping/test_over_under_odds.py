import unittest

from scraping.over_under_odds import find_minimum_spread


class TestOverUnderOdds(unittest.TestCase):
    def test_find_minimum_spread(self):
        """Checks that find_minimum_spread() returns a dictionary with the line
        containing the lowest spread between over and under odds.
        """
        # Sheffield United - Everton, 2 Sep 2023, 2-2
        ou_lines = {
            '2': [1.5, 2.75],
            '2.25': [1.75, 2.18],
            '2.5': [2.05, 1.87],
            '2.75': [2.29, 1.69],
            '3': [2.82, 1.48],
        }
        ou_line = find_minimum_spread(ou_lines)
        self.assertDictEqual(ou_line, {'2.5': [2.05, 1.87]})
