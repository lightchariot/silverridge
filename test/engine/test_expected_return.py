import unittest

from engine.poisson import expected_return


class TestExpectedReturn(unittest.TestCase):
	def test_calculation(self):
		self.assertAlmostEqual(expected_return(2, 0.5), 0)
		self.assertAlmostEqual(expected_return(3.5, 0.2), -0.3)

	def test_calculation_with_specified_loss(self):
		self.assertAlmostEqual(expected_return(1.9, 0.45, p_loss=0.4), 0.005)
