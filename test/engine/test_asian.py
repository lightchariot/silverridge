import unittest

from engine.asian import get_line_pair


class TestAsian(unittest.TestCase):
    def test_get_line_pair_25(self):
        """Checks that get_line_pair() produces the two correct total lines
        when the Asian total ends in .25.
        """
        t1, t2 = get_line_pair(2.25)
        self.assertEqual(t1, 2)
        self.assertEqual(t2, 2.5)

    def test_get_line_pair_75(self):
        """Checks that get_line_pair() produces the two correct total lines
        when the Asian total ends in .75.
        """
        t1, t2 = get_line_pair(2.75)
        self.assertEqual(t1, 2.5)
        self.assertEqual(t2, 3)
