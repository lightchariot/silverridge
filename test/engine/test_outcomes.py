import unittest

from engine.poisson import totals_probabilities


class TestOutcomes(unittest.TestCase):
	def setUp(self):
		# Probabilities for:
		# home mean goals = 1.4
		# away mean goals = 1.1
		self.probabilities = [
			[0.08208, 0.09029, 0.04966, 0.01821, 0.00501, 0.00110],
			[0.11492, 0.12641, 0.06953, 0.02549, 0.00701, 0.00154],
			[0.08044, 0.08849, 0.04867, 0.01785, 0.00491, 0.00108],
			[0.03754, 0.04129, 0.02271, 0.00833, 0.00229, 0.00050],
			[0.01314, 0.01445, 0.00795, 0.00291, 0.00080, 0.00018],
			[0.00368, 0.00405, 0.00222, 0.00082, 0.00022, 0.00005]]

	def test_totals_no_push(self):
		"""Checks that totals_probabilities() correctly computes the over and
		under probabilities when given a non-integer total as the O/U line.
		"""
		# o/u 2.5 matrix:
		# u u u o o o
		# u u o o o o
		# u o o o o o
		# o o o o o o
		# o o o o o o
		# o o o o o o
		over, under = totals_probabilities(self.probabilities, 2.5)
		self.assertAlmostEqual(over, 0.4562)
		self.assertAlmostEqual(under, 0.5438)

	def test_totals_with_push(self):
		"""Checks that totals_probabilities() correctly computes the over and
		under probabilities when given an integer total as the O/U line.
		"""
		# o/u 2 matrix:
		# u u p o o o
		# u p o o o o
		# p o o o o o
		# o o o o o o
		# o o o o o o
		# o o o o o o
		over, under = totals_probabilities(self.probabilities, 2)
		self.assertAlmostEqual(over, 0.4562)
		self.assertAlmostEqual(under, 0.28729)
