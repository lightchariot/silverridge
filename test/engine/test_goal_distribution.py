import unittest
import numpy.testing as npt
from engine.poisson import goals_dist


class TestGoalDistribution(unittest.TestCase):
	def test_distribution(self):
		"""Checks that goals_dist() produces the correct Poisson distribution,
		given a mean number of goals.
		"""
		mean = 2
		distribution = goals_dist(mean)
		npt.assert_almost_equal(distribution, [0.135335,  # 0 goals
		                                       0.270671,  # 1 goal
		                                       0.270671,  # 2 goals
		                                       0.180447,  # ...
		                                       0.090224,
		                                       0.036089,
		                                       0.01203,
		                                       0.003437,
		                                       0.000859,  # 8 goals
		                                       0.000191,  # 9 goals
		                                       ], decimal=6)
