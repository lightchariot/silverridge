import unittest

from engine.poisson import make_game_matrix


class TestGameMatrix(unittest.TestCase):
	def test_make_game_matrix(self):
		probabilities = make_game_matrix(home_mean=1.4, away_mean=1.1)

		# Check the probabilities of a 0-0 draw, a 2-1 home win, and
		# a 0-1 away win, respectively.
		self.assertAlmostEqual(probabilities[0][0], 0.08208, delta=0.001)
		self.assertAlmostEqual(probabilities[2][1], 0.08849, delta=0.001)
		self.assertAlmostEqual(probabilities[0][1], 0.09029, delta=0.001)
