import argparse
import csv
import math

import matplotlib.pyplot as plt
import numpy as np


def variance(dist):
    """Calculates the variance of a given distribution.

    The distribution is provided as counts. For example, the following
    distribution

    [12, 23, 17, 8, 4]

    indicates that there were
    * 12 instances of 0,
    * 23 instances of 1,

    and so on.
    """
    arr = []
    for g, ct in enumerate(dist):
        arr += [g] * ct
    return np.var(arr)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--all_matches_file', type=str)
    args = parser.parse_args()

    goals_dist = [0] * 20
    team_gp = 0
    goals = 0
    with open(args.all_matches_file) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            home_goals = int(row['home_goals'])
            away_goals = int(row['away_goals'])
            goals_dist[home_goals] += 1
            goals_dist[away_goals] += 1
            goals += (home_goals + away_goals)
            team_gp += 2
    g_mean = goals / team_gp
    var = variance(goals_dist)
    print('Mean goals scored per side: %.3f' % g_mean)
    print('Variance: %.3f' % var)
    print('Mean-variance spread: %.3f' % abs(g_mean - var))
    max_goals = 0
    for g in range(len(goals_dist)):
        if goals_dist[g] > 0:
            max_goals = g
    goals_dist = goals_dist[:max_goals + 1]

    expected = []
    for g in range(len(goals_dist)):
        p_g = (g_mean**g * math.e**-g_mean) / math.factorial(g)
        print('E(%d goals) = %.2f, Actual = %d' % (g, team_gp * p_g, goals_dist[g]))
        expected.append(team_gp * p_g)

    plt.bar(list(range(len(goals_dist))), goals_dist, zorder=1)
    plt.scatter(list(range(len(expected))), expected, zorder=2)
    plt.title('Number of Goals Scored By Teams')
    plt.xlabel('Number of Goals Scored')
    plt.ylabel('Count')
    plt.legend(['Expected', 'Actual'])
    plt.show()
