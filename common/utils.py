import csv
from ast import literal_eval

import requests
from bs4 import BeautifulSoup


def parse_html(url):
    r = requests.get(url)
    html_dict = literal_eval(r.text)
    raw_html = html_dict['odds']
    raw_html = raw_html.replace('&lt;', '<')
    raw_html = raw_html.replace('&gt;', '>')
    raw_html = raw_html.replace('\\', '')
    return BeautifulSoup(raw_html, 'html.parser')


def make_data(filename):
    with open(filename, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        return [row for row in reader]
