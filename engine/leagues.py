def get_league_totals(league_data):
	"""Calculates the league's total number of home goals scored,
	away goals scored, and games played.
	
	Parameters:
	-----------
	league_data: a dict of dicts
		Each key is a team, and each value contains a dictionary of that
		team's goals scored & allowed, both home and away.

	Returns:
	--------
	home_goals: int
		The sum of all home goals scored in the league.
	away_goals: int
		The sum of all away goals_scored in the league.
	games_played: int
		The total number of games played in the league.
	"""
	home_goals = sum(int(data['home_gf']) for _, data in league_data.items())
	away_goals = sum(int(data['home_ga']) for _, data in league_data.items())
	games_played = sum(int(data['home_gp']) for _, data in league_data.items())
	return home_goals, away_goals, games_played


def make_league_standings(results):
	"""TODO(rohit): What does this do?

	Parameters:
	-----------
	results: list of dicts

		Columns:
			* kickoff_time,
			* home_team,
			* away_team,
			* home_goals,
			* away_goals

	Returns:
	--------
	dict
	"""
	league_standings = {}
	for result in results:
		home_team = result['home_team']
		away_team = result['away_team']
		if home_team not in league_standings:
			league_standings[home_team] = {
				'home_gp': 0, 'home_gf': 0, 'home_ga': 0,
				'away_gp': 0, 'away_gf': 0, 'away_ga': 0,
			}
		if away_team not in league_standings:
			league_standings[away_team] = {
				'home_gp': 0, 'home_gf': 0, 'home_ga': 0,
				'away_gp': 0, 'away_gf': 0, 'away_ga': 0,
			}
		home_goals = int(result['home_goals'])
		away_goals = int(result['away_goals'])
		league_standings[home_team]['home_gp'] += 1
		league_standings[home_team]['home_gf'] += home_goals
		league_standings[home_team]['home_ga'] += away_goals
		league_standings[away_team]['away_gp'] += 1
		league_standings[away_team]['away_gf'] += away_goals
		league_standings[away_team]['away_ga'] += home_goals
	return league_standings
