def is_asian(line):
    return str(line)[-2:] == '25' or str(line)[-2:] == '75'


def get_line_pair(line):
    """Finds the pair of lines that an Asian total straddles.

    Asian totals never fall on whole numbers or halves, always on quarters,
    e.g. 1.75 or 3.25.

    For example, if an Asian total is set at
    * 2.25, then the line pair is 2, 2.5.
    * 3.75, then the line pair is 3.5, 4.
    """
    line_lo = line - 0.25
    line_hi = line + 0.25
    if str(line)[-2:] == '25':
        line_lo = int(round(line_lo))
    if str(line)[-2:] == '75':
        line_hi = int(round(line_hi))
    return line_lo, line_hi
